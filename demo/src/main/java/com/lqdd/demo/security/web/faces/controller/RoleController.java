
package com.lqdd.demo.security.web.faces.controller;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.lqdd.demo.framework.web.faces.annotation.MessageRequired;
import com.lqdd.demo.framework.web.faces.annotation.MessageType;
import com.lqdd.demo.framework.web.faces.controller.FacesController;
import com.lqdd.demo.security.domain.Role;
import com.lqdd.demo.security.service.ResourceService;
import com.lqdd.demo.security.service.RoleService;
import com.lqdd.demo.security.web.model.RoleDataModel;

@Controller("/views/security/role.faces")
public class RoleController extends FacesController {

	@Autowired
	private RoleService roleService;

	@Autowired
	private ResourceService resourceService;

	@Override
	public void onEntry() {
		putViewScope("resources", resourceService.findTree());
		find();
	}

	public LazyDataModel<Role> getDataModel() {
		RoleDataModel dataModel = RoleDataModel.getInstance();
		return dataModel;
	}

	private void find() {
		putViewScope("selectedRole", new Role());
		putViewScope("selectedRoles", new Role[0]);
	}

	public void add() {
		putViewScope("role", new Role());
	}

	@MessageRequired(type = MessageType.SAVE)
	public void save() {
		Role role = (Role) getViewScope("role");
		roleService.save(role);
	}

	public void edit() {
		Role[] roles = (Role[]) getViewScope("selectedRoles");
		Role role = roles.length > 0 ? roles[0] : null;
		putViewScope("role", role);
	}

	@MessageRequired(type = MessageType.DELETE)
	public void delete() {
		Role[] roles = (Role[]) getViewScope("selectedRoles");
		roleService.delete(roles);
	}

	public void onRowSelect(SelectEvent event) {
		Role role = (Role) event.getObject();
		putViewScope("resources", resourceService.findSeleciontTree(role.getResourceIds()));
		putViewScope("selectedRole", role);
	}

	@MessageRequired(type = MessageType.OPERATION)
	public void onClickUpdateResource() {
		Role role = (Role) getViewScope("selectedRole");
		roleService.updateResourceRelation(role);
		putViewScope("resources", resourceService.findSeleciontTree(role.getResourceIds()));
	}
}