package com.lqdd.demo.security.service;

import java.util.List;
import java.util.Set;

import org.primefaces.model.TreeNode;

import com.lqdd.demo.framework.service.BaseService;
import com.lqdd.demo.security.domain.User;

public interface UserService extends BaseService<User> {

	public User findByUsername(String username);

	public Set<String> findRoles(String username);

	public Set<String> findPermissions(String username);

	public void updateRoleRelation(User user);
	
	public TreeNode findTree();
	
	public TreeNode findSeleciontTree(List<Long> selectedIds);

}
