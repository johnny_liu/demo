package com.lqdd.demo.security.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.primefaces.model.TreeNode;

import com.lqdd.demo.framework.domain.AuditableEntity;

public class Role extends AuditableEntity {

	private static final long serialVersionUID = 4391038509751614566L;

	// 角色编码
	@NotBlank
	@Length(max = 16)
	private String code;

	// 角色名称
	@NotBlank
	@Length(max = 32)
	private String name;

	// 角色类型
	@NotBlank
	@Length(max = 16)
	private String type;

	// 角色描述
	@Length(max = 500)
	private String description;

	// 是否可用
	private boolean enable = true;

	// 角色资源
	private List<Resource> resources;

	private TreeNode[] selectedResources;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public List<Resource> getResources() {
		if (resources == null) {
			resources = new ArrayList<>();
		}
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public TreeNode[] getSelectedResources() {
		if (selectedResources == null) {
			selectedResources = new TreeNode[0];
		}
		return selectedResources;
	}

	public void setSelectedResources(TreeNode[] selectedResources) {
		if (selectedResources != null) {
			Set<Resource> datas = new HashSet<>();
			for (TreeNode treeNode : selectedResources) {
				Resource resource = (Resource) treeNode.getData();
				datas.add(resource);
			}
			this.setResources(new ArrayList<>(datas));
		}
		this.selectedResources = selectedResources;
	}

	public List<Long> getResourceIds() {
		List<Long> resourceIds = new ArrayList<>();
		for (Resource resource : getResources()) {
			resourceIds.add(resource.getId());
		}
		return resourceIds;
	}

}
