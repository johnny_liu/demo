package com.lqdd.demo.security.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.model.TreeNode;

import com.lqdd.demo.framework.service.BaseService;
import com.lqdd.demo.security.domain.Resource;

public interface ResourceService extends BaseService<Resource> {

	public TreeNode findTree();

	public List<Resource> findInfo(Map<String, Object> sreachParam);

	public TreeNode findSeleciontTree(List<Long> selections);

	public Set<String> findPermissions(Set<Long> resourceIds);

	public List<Resource> findByRoleId(Long roleId);

	public void findShiroPermissions(Map<String, String> filterChainDefinitionMap);
	
}
