package com.lqdd.demo.security.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.lqdd.demo.framework.persistence.BaseDao;
import com.lqdd.demo.security.domain.Role;

@Mapper
public interface RoleDao extends BaseDao<Role> {

	public List<Role> findByUserId(@Param("userId") Long userId);

	public int insertRelation(Role role);

	public int deleteRelation(@Param("id") Long id);

	public List<Long> findUserIdByRoleId(Long roleId);
}