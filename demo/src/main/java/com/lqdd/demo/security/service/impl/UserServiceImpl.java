/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.security.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lqdd.demo.security.dao.UserDao;
import com.lqdd.demo.security.domain.User;
import com.lqdd.demo.security.service.RoleService;
import com.lqdd.demo.security.service.UserService;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleService roleService;

	@Override
	public Set<String> findRoles(String username) {
		User user = findByUsername(username);
		if (user == null) {
			return Collections.emptySet();
		}
		return roleService.findRoles(user.getRoleIds().toArray(new Long[0]));
	}

	@Override
	public Set<String> findPermissions(String username) {
		User user = findByUsername(username);
		if (user == null) {
			return Collections.emptySet();
		}
		return roleService.findPermissions(user.getRoleIds().toArray(new Long[0]));
	}

	@Override
	public List<User> findAll(Map<String, Object> sreachParam) {
		return userDao.query(sreachParam);
	}

	@Override
	public User findOne(Map<String, Object> sreachParam) {
		List<User> users = findAll(sreachParam);
		return users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public void save(User user) {
		if (user.getId() == null) {
			user.setOperator("admin");
			user.setOperationTime(new Date());
			userDao.insert(user);
		} else {
			userDao.update(user);
		}

	}

	@Override
	public void delete(User[] users) {
		for (User user : users) {
			userDao.delete(user.getId());
		}
	}

	@Override
	public User findById(Long id) {
		return userDao.get(id);
	}

	@Override
	public void updateRoleRelation(User user) {
		if (user.getId() != null) {
			userDao.deleteRelation(user.getId());
			userDao.insertRelation(user);
		}
	}
	
	@Override
	public TreeNode findTree() {
		return findSeleciontTree(null);
	}

	@Override
	public TreeNode findSeleciontTree(List<Long> selectedIds) {
		List<User> users = findAll(null);
		TreeNode root = new CheckboxTreeNode(null);
		for (User user : users) {
			TreeNode node = new CheckboxTreeNode(user, root);
			node.setSelected(selectedIds != null && selectedIds.contains(user.getId()));
		}
		return root;
	}
}