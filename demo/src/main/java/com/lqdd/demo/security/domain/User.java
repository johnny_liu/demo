package com.lqdd.demo.security.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.primefaces.model.TreeNode;

import com.lqdd.demo.common.utils.Digests;
import com.lqdd.demo.common.utils.Encodes;
import com.lqdd.demo.framework.domain.AuditableEntity;

public class User extends AuditableEntity {

	private static final long serialVersionUID = -3902212171908439082L;

	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;
	// 用户名
	@NotBlank
	@Length(max = 32)
	private String username;

	// 密码
	@Length(max = 64)
	private String password;

	// 加密密码的盐
	@Length(max = 16)
	private String salt;

	// 联系电话
	@Length(max = 100)
	private String mobile;

	// 联系邮箱
	@Length(max = 100)
	private String email;

	// 用户是否锁定
	private boolean locked;

	// 描述
	@Length(max = 500)
	private String description;

	// 拥有的角色列表
	private List<Role> roles;

	private TreeNode[] selectedRoles;

	private String plainPassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Role> getRoles() {
		if (roles == null) {
			roles = new ArrayList<>();
		}
		return roles;
	}

	public List<Long> getRoleIds() {
		List<Long> roleIds = new ArrayList<>();
		for (Role role : getRoles()) {
			roleIds.add(role.getId());
		}
		return roleIds;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		this.setSalt(Encodes.encodeHex(salt));
		this.setPassword(entryptPassword(plainPassword, salt));
		this.plainPassword = plainPassword;
	}

	public TreeNode[] getSelectedRoles() {
		if (selectedRoles == null) {
			selectedRoles = new TreeNode[0];
		}
		return selectedRoles;
	}

	public void setSelectedRoles(TreeNode[] selectedRoles) {
		if (selectedRoles != null) {
			Set<Role> datas = new HashSet<>();
			for (TreeNode treeNode : selectedRoles) {
				datas.add((Role) treeNode.getData());
			}
			this.setRoles(new ArrayList<>(datas));
		}
		this.selectedRoles = selectedRoles;
	}

	public byte[] getCredentialsSalt() {
		return Encodes.decodeHex(getSalt());
	}

	public String entryptPassword(String plainPassword, byte[] salt) {
		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
		String password = Encodes.encodeHex(hashPassword);
		return password;
	}

}
