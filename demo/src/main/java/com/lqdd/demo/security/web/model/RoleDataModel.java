package com.lqdd.demo.security.web.model;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lqdd.demo.framework.web.faces.utils.FacesContextUtils;
import com.lqdd.demo.security.dao.RoleDao;
import com.lqdd.demo.security.domain.Role;

public class RoleDataModel extends LazyDataModel<Role> {

	private static final long serialVersionUID = 2957926997919683676L;

	private RoleDataModel() {
	}

	private static class InnerRoleDataModel {
		private static final RoleDataModel dataModel = new RoleDataModel();
	}

	public static RoleDataModel getInstance() {
		return InnerRoleDataModel.dataModel;
	}

	public RoleDao getRoleDao() {
		return FacesContextUtils.getBean(RoleDao.class);
	}

	@Override
	public Role getRowData(String rowKey) {
		return getRoleDao().get(Long.valueOf(rowKey));
	}

	@Override
	public List<Role> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		Page<Role> page = PageHelper.startPage(getPage(first, pageSize), pageSize)
				.setOrderBy(getOrderSql(sortField, sortOrder));
		List<Role> roles = getRoleDao().query(filters);
		this.setRowCount((int) page.getTotal());
		return roles;
	}

	@Override
	public void setRowIndex(int rowIndex) {
		super.setRowIndex((getPageSize() == 0) ? -1 : rowIndex);
	}

	private int getPage(int first, int pageSize) {
		int pageNum = (pageSize > 0) ? (first / pageSize) : 0;
		return pageNum + 1;
	}

	private String getOrderSql(String sortField, SortOrder sortOrder) {
		if (StringUtils.isNotBlank(sortField)) {
			String sql = sortField + " ";
			if (SortOrder.ASCENDING.equals(sortOrder)) {
				return sql + "asc";
			} else if (SortOrder.DESCENDING.equals(sortOrder)) {
				return sql + "desc";
			}
		}
		return "";
	}

}
