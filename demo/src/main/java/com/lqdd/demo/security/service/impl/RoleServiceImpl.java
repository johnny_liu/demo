/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.security.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lqdd.demo.security.dao.RoleDao;
import com.lqdd.demo.security.domain.Role;
import com.lqdd.demo.security.service.ResourceService;
import com.lqdd.demo.security.service.RoleService;

@Service
@Transactional(readOnly = true)
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private ResourceService resourceService;

	@Override
	public Role findById(Long id) {
		return roleDao.get(id);
	}

	@Override
	public List<Role> findAll(Map<String, Object> sreachParam) {
		return roleDao.query(sreachParam);
	}

	@Override
	public Role findOne(Map<String, Object> sreachParam) {
		List<Role> roles = findAll(sreachParam);
		return roles.size() > 0 ? roles.get(0) : null;
	}

	@Override
	public Set<String> findRoles(Long... roleIds) {
		Set<String> roles = new HashSet<String>();
		for (Long roleId : roleIds) {
			Role role = roleDao.get(roleId);
			if (role != null) {
				roles.add(role.getType());
			}
		}
		return roles;
	}

	@Override
	public Set<String> findPermissions(Long... roleIds) {
		Set<Long> resourceIds = new HashSet<Long>();
		for (Long roleId : roleIds) {
			Role role = roleDao.get(roleId);
			if (role != null) {
				resourceIds.addAll(role.getResourceIds());
			}
		}
		return resourceService.findPermissions(resourceIds);
	}

	@Override
	public void save(Role role) {
		role.setOperator("admin");
		role.setOperationTime(new Date());
		if (role.getId() == null) {
			roleDao.insert(role);
		} else {
			roleDao.update(role);
		}
	}

	@Override
	@Transactional
	public void updateResourceRelation(Role role) {
		if (role.getId() != null) {
			roleDao.deleteRelation(role.getId());
			if (role.getResourceIds().size() != 0) {
				roleDao.insertRelation(role);
			}
		}
	}

	@Override
	public void delete(Role[] roles) {
		for (Role role : roles) {
			roleDao.delete(role.getId());
		}
	}

	@Override
	public TreeNode findTree() {
		return findSeleciontTree(null);
	}

	@Override
	public TreeNode findSeleciontTree(List<Long> roleIds) {
		List<Role> roles = findAll(null);
		TreeNode root = new CheckboxTreeNode(null);
		for (Role role : roles) {
			TreeNode node = new CheckboxTreeNode(role, root);
			node.setSelected(roleIds != null && roleIds.contains(role.getId()));
		}
		return root;
	}

	@Override
	public List<Long> findUserIdByRoleId(Long roleId) {
		return roleDao.findUserIdByRoleId(roleId);
	}
}