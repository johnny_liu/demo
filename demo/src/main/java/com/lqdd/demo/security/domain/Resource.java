package com.lqdd.demo.security.domain;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.lqdd.demo.common.utils.EnumUtil;
import com.lqdd.demo.framework.domain.AuditableEntity;

public class Resource extends AuditableEntity {

	private static final long serialVersionUID = 8032268871677473656L;

	public static enum Type {
		MENU, BUTTON;

		private final String name;

		private Type() {
			this.name = EnumUtil.getMessage(this, "messages");
		}

		public String getName() {
			return name;
		}
	}

	// 父节点id
	@Length(max = 32)
	private Long parentId;

	// 资源编码
	@NotBlank
	@Length(max = 32)
	private String code;

	// 资源名称
	@NotBlank
	@Length(max = 64)
	private String name;

	// 资源类型 M:菜单 B:按钮
	private Type type = Type.MENU;

	// URL 请求链接
	@Length(max = 512)
	private String url;

	// 排序
	private Integer seq;

	// 权限
	private String permission;

	// 图标
	@Length(max = 32)
	private String icon;

	// 可使用
	@NotNull
	private boolean enable = true;

	// 是否叶子节点
	@NotNull
	private boolean leaf;

	// 描述
	@Length(max = 512)
	private String description;

	// 子资源
	private List<Resource> children;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getUrl() {
		if (StringUtils.isBlank(this.url)) {
			return "";
		}
		return url;
	}

	public String getOpenUrl() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context != null) {
			return context.getExternalContext().getRequestContextPath() + url;
		}
		return "";
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isLeaf() {
		if (children == null)
			return true;

		return children.isEmpty();
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public List<Resource> getChildren() {
		return children;
	}

	public void setChildren(List<Resource> children) {
		this.children = children;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
