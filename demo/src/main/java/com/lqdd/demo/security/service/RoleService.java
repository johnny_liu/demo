package com.lqdd.demo.security.service;

import java.util.List;
import java.util.Set;

import org.primefaces.model.TreeNode;

import com.lqdd.demo.framework.service.BaseService;
import com.lqdd.demo.security.domain.Role;

public interface RoleService extends BaseService<Role> {

	public TreeNode findTree();

	public TreeNode findSeleciontTree(List<Long> roleIds);

	public Set<String> findRoles(Long... roleIds);

	public Set<String> findPermissions(Long... roleIds);

	public void updateResourceRelation(Role role);

	public List<Long> findUserIdByRoleId(Long roleId);
}
