
package com.lqdd.demo.security.web.faces.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.lqdd.demo.framework.web.faces.annotation.MessageRequired;
import com.lqdd.demo.framework.web.faces.annotation.MessageType;
import com.lqdd.demo.framework.web.faces.controller.FacesController;
import com.lqdd.demo.security.domain.User;
import com.lqdd.demo.security.service.RoleService;
import com.lqdd.demo.security.service.UserService;

@Controller("/views/security/user.faces")
public class UserController extends FacesController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Override
	public void onEntry() {
		putViewScope("roles", roleService.findTree());
		find();
	}

	@RequiresPermissions(value = { "security/user:view" })
	public void find() {
		putViewScope("selectedUser", new User());
		putViewScope("users", userService.findAll(null));
	}

	public void add() {
		User user = new User();
		putViewScope("user", user);
	}

	@MessageRequired(type = MessageType.SAVE)
	public void save() {
		User user = (User) getViewScope("user");
		userService.save(user);
		find();
	}

	public void edit() {
		User user = (User) getViewScope("selectedUser");
		putViewScope("user", user);
	}

	@MessageRequired(type = MessageType.DELETE)
	public void delete() {
		User user = (User) getViewScope("selectedUser");
		userService.delete(new User[] { user });
		find();
	}

	public void onRowSelect(SelectEvent event) {
		User user = (User) event.getObject();
		user = userService.findById(user.getId());
		putViewScope("roles", roleService.findSeleciontTree(user.getRoleIds()));
	}

}