package com.lqdd.demo.security.dao;

import org.apache.ibatis.annotations.Mapper;

import com.lqdd.demo.framework.persistence.BaseDao;
import com.lqdd.demo.security.domain.User;

@Mapper
public interface UserDao extends BaseDao<User> {

	public User findByUsername(String username);

	public int deleteRelation(Long userId);

	public int insertRelation(User user);
}