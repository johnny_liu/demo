
package com.lqdd.demo.security.web.faces.controller;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.lqdd.demo.framework.web.faces.annotation.MessageRequired;
import com.lqdd.demo.framework.web.faces.annotation.MessageType;
import com.lqdd.demo.framework.web.faces.controller.FacesController;
import com.lqdd.demo.security.domain.Resource;
import com.lqdd.demo.security.service.ResourceService;

@Controller("/views/security/resource.faces")
public class ResourceController extends FacesController {

	@Autowired
	private ResourceService resourceService;

	@Override
	public void onEntry() {
		putViewScope("types", Resource.Type.values());
		find();
	}

	private void find() {
		putViewScope("resources", resourceService.findTree());
		putViewScope("selectedResources", new TreeNode[0]);
	}

	public void add() {
		Resource parentResource = selectedOneResource();
		Resource resource = new Resource();
		if (parentResource != null) {
			resource.setParentId(parentResource.getId());
		}
		putViewScope("parentResource", parentResource);
		putViewScope("resource", resource);
	}

	@MessageRequired(type = MessageType.SAVE)
	public void save() {
		Resource resource = (Resource) getViewScope("resource");
		resourceService.save(resource);
		find();
	}

	public void edit() {
		Resource resource = selectedOneResource();
		Resource parentResource = resourceService.findById(resource.getParentId());
		putViewScope("resource", resource);
		putViewScope("parentResource", parentResource);
	}

	@MessageRequired(type = MessageType.DELETE)
	public void delete() {
		TreeNode[] nodes = (TreeNode[]) getViewScope("selectedResources");
		Resource[] resources = new Resource[nodes.length];
		for (int i = 0; i < nodes.length; i++) {
			resources[i] = (Resource) nodes[i].getData();
		}
		resourceService.delete(resources);
		find();
	}

	private Resource selectedOneResource() {
		TreeNode[] nodes = (TreeNode[]) getViewScope("selectedResources");
		if (nodes != null) {
			TreeNode node = nodes.length > 0 ? nodes[0] : new DefaultTreeNode();
			return (Resource) node.getData();
		}
		return null;
	}
}