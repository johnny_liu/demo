/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.security.service.impl;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lqdd.demo.security.dao.ResourceDao;
import com.lqdd.demo.security.domain.Resource;
import com.lqdd.demo.security.service.ResourceService;

@Service
@Transactional(readOnly = true)
public class ResourceServiceImpl implements ResourceService {

	public static final String PREMISSION_STRING = "perms[\"{0}\"]";

	@Autowired
	private ResourceDao resourceDao;

	@Override
	public Resource findById(Long id) {
		return resourceDao.get(id);
	}

	@Override
	public List<Resource> findAll(Map<String, Object> sreachParam) {
		if (sreachParam == null) {
			sreachParam = new HashMap<>();
		}
		return resourceDao.query(sreachParam);
	}

	@Override
	public Resource findOne(Map<String, Object> sreachParam) {
		List<Resource> resources = findAll(sreachParam);
		return resources.size() > 0 ? resources.get(0) : null;
	}

	@Override
	public Set<String> findPermissions(Set<Long> resourceIds) {
		Set<String> permissions = new HashSet<String>();
		for (Long resourceId : resourceIds) {
			Resource resource = resourceDao.get(resourceId);
			if (resource != null && StringUtils.isNotBlank(resource.getPermission())) {
				permissions.add(resource.getPermission().trim());
			}
		}
		return permissions;
	}

	@Override
	public void save(Resource resource) {
		resource.setOperator("admin");
		resource.setOperationTime(new Date());

		if (resource.getId() == null) {
			resourceDao.insert(resource);
		} else {
			resourceDao.update(resource);
		}
	}

	@Override
	public void delete(Resource[] resources) {
		for (Resource resource : resources) {
			resourceDao.delete(resource.getId());
		}
	}

	@Override
	public List<Resource> findByRoleId(Long roleId) {
		return resourceDao.findByRoleId(roleId);
	}

	@Override
	public TreeNode findTree() {
		return findSeleciontTree(null);
	}

	@Override
	public List<Resource> findInfo(Map<String, Object> sreachParam) {
		return resourceDao.queryRoot(sreachParam);
	}

	@Override
	public TreeNode findSeleciontTree(List<Long> selections) {
		List<Resource> resources = findInfo(null);
		TreeNode root = new CheckboxTreeNode(null);
		setTreeLeaf(resources, root, selections);
		return root;
	}

	private void setTreeLeaf(List<Resource> resources, TreeNode root, List<Long> selections) {
		root.setExpanded(true);
		for (Resource resource : resources) {
			TreeNode node = new CheckboxTreeNode(resource, root);
			if (resource.isLeaf()) {
				node.setSelected(selections != null && selections.contains(resource.getId()));
			}
			setTreeLeaf(resource.getChildren(), node, selections);
		}
	}

	@Override
	public void findShiroPermissions(Map<String, String> filterChainDefinitionMap) {
		List<Resource> resList = findAll(null);
		for (Resource res : resList) {
			if (StringUtils.isNotBlank(res.getPermission()) && StringUtils.isNotBlank(res.getUrl())) {
				filterChainDefinitionMap.put(res.getUrl(),
						MessageFormat.format(PREMISSION_STRING, res.getPermission().trim()));
			}
		}
	}
}