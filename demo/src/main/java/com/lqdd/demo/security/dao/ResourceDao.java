package com.lqdd.demo.security.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.lqdd.demo.framework.persistence.BaseDao;
import com.lqdd.demo.security.domain.Resource;

@Mapper
public interface ResourceDao extends BaseDao<Resource> {

	public List<Resource> findByRoleId(@Param("roleId") Long roleId);

	public List<Resource> queryRoot(Map<String, Object> param);

}