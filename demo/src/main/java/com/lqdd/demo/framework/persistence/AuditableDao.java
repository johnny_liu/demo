package com.lqdd.demo.framework.persistence;

import com.lqdd.demo.framework.domain.AuditableEntity;

public interface AuditableDao<T extends AuditableEntity> extends BaseDao<T> {

}
