package com.lqdd.demo.framework.web.faces.aspect;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;

import com.lqdd.demo.framework.exception.MessageResolvableException;
import com.lqdd.demo.framework.web.faces.annotation.MessageRequired;
import com.lqdd.demo.framework.web.faces.utils.RequestContextUtils;

@Aspect
@Configuration
public class MessageAspect {

	@Autowired
	private MessageSource messageSource;

	private String messageCodePrefix = "";

	public MessageAspect() {
		messageCodePrefix = "framework.";
	}

	public void setMessageCodePrefix(String messageCodePrefix) {
		this.messageCodePrefix = messageCodePrefix;
	}

	@Around("@annotation(messageRequired)")
	public Object showMessage(ProceedingJoinPoint joinPoint, MessageRequired messageRequired) throws Throwable {
		try {
			Object result = joinPoint.proceed();
			String messageCode = this.messageCodePrefix + messageRequired.type().getSuccessCode();
			createFacesMessage(FacesMessage.SEVERITY_INFO, messageCode, null, new Object[] { result });
			renderSuccessMessages();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			String messageCode = this.messageCodePrefix + messageRequired.type().getFailedCode();
			String messageDetail = null;
			if ((e instanceof MessageResolvableException)) {
				MessageResolvableException e1 = (MessageResolvableException) e;
				messageDetail = this.messageSource.getMessage(e1.getCategory() + "." + e1.getErrorCode(),
						e1.getParameters(), getResponse().getLocale());
			} else {
				messageDetail = this.messageSource.getMessage(messageCodePrefix + "contactTheAdmin", null,
						getResponse().getLocale());
			}
			createFacesMessage(FacesMessage.SEVERITY_ERROR, messageCode, messageDetail, null);
			renderFailedMessages();
		}
		return null;
	}

	private void createFacesMessage(Severity severity, String title, String messageDetail, Object[] results) {
		String message = this.messageSource.getMessage(title, results, getResponse().getLocale());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(severity, message, messageDetail));
	}

	private HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	protected void renderSuccessMessages() {
		RequestContextUtils.update("globalMessageGrowl");
	}

	protected void renderFailedMessages() {
		RequestContextUtils.executeAndUpdate("PF('globalMessageDialog').show();", "globalMessages");
	}

}
