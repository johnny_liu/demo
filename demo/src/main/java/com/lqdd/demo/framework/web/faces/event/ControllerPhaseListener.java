package com.lqdd.demo.framework.web.faces.event;

import javax.faces.component.UIViewRoot;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import com.lqdd.demo.framework.web.faces.controller.FacesController;

//jsf生命周期
public class ControllerPhaseListener implements PhaseListener {

	private static final long serialVersionUID = -4016428550782703448L;

	@Override
	public void beforePhase(PhaseEvent event) {
		UIViewRoot view = event.getFacesContext().getViewRoot();
		if (view.getViewMap(false) == null) {
			FacesController controller = FacesController.getCurrentInstance();
			if (controller != null) {
				controller.onEntry();
			}
		}
	}

	@Override
	public void afterPhase(PhaseEvent event) {
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}

}
