package com.lqdd.demo.framework.domain;

import java.io.Serializable;


public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = -8774370128378359009L;


	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
