package com.lqdd.demo.framework.web.faces.el;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.PropertyNotWritableException;

import org.springframework.stereotype.Component;

import com.lqdd.demo.framework.web.faces.controller.FacesController;

@Component
public class FacesControllerELResolver extends ELResolver {

	static final String FACES_CONTROLLER_KEY = "controller";

	@Override
	public Class<?> getCommonPropertyType(ELContext context, Object base) {

		return FacesController.class;
	}

	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
		return null;
	}

	@Override
	public Class<?> getType(ELContext context, Object base, Object property) {
		if ((base == null) && (FACES_CONTROLLER_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return FacesController.class;
		}
		return null;
	}

	@Override
	public Object getValue(ELContext context, Object base, Object property) {
		if ((base == null) && (FACES_CONTROLLER_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return FacesController.getCurrentInstance();
		}
		return null;
	}

	@Override
	public boolean isReadOnly(ELContext context, Object base, Object property) {
		if ((base == null) && (FACES_CONTROLLER_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return true;
		}
		return false;
	}

	@Override
	public void setValue(ELContext context, Object base, Object property, Object value) {
		if ((base == null) && (FACES_CONTROLLER_KEY.equals(property))) {
			throw new PropertyNotWritableException("The 'controller' variable is not writable.");
		}
	}

}
