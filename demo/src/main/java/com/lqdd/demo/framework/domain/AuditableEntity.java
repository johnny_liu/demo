package com.lqdd.demo.framework.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public abstract class AuditableEntity extends BaseEntity {

	private static final long serialVersionUID = 4653746083590934384L;

	public static class OperationLog {
		private Date operationTime;
		private String operator;

		public OperationLog(Date operationTime, String operator) {
			this.operationTime = operationTime;
			this.operator = operator;
		}

		public Date getOperationTime() {
			return this.operationTime;
		}

		public String getOperator() {
			return this.operator;
		}
	}

	@Size(max = 1)
	protected boolean delflag;

	@NotNull
	private Date operationTime;

	@Size(max = 300)
	@NotNull
	private String operator;

	public static final ThreadLocal<OperationLog> operationLogThreadLocal = new ThreadLocal<>();

	public Date getOperationTime() {
		return operationTime;
	}

	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getCreatedTime() {
		return null;
	}

	public String getCreator() {
		return null;
	}

	public void setCreatedTime(Date createdTime) {
	}

	public void setCreator(String creator) {
	}

	public boolean isDelflag() {
		return delflag;
	}

	public void setDelflag(boolean delflag) {
		this.delflag = delflag;
	}

	public void prePersist() {
		OperationLog log = (OperationLog) operationLogThreadLocal.get();
		if (log != null) {
			setOperationTime(log.getOperationTime());
			setOperator(log.getOperator());
			if (getCreatedTime() == null) {
				setCreatedTime(log.getOperationTime());
			}
			if (getCreator() == null) {
				setCreator(log.getOperator());
			}
		}
	}

	public void preUpdate() {
		OperationLog log = (OperationLog) operationLogThreadLocal.get();
		if (log != null) {
			setOperationTime(log.getOperationTime());
			setOperator(log.getOperator());
			if (getCreatedTime() == null) {
				setCreatedTime(log.getOperationTime());
			}
			if (getCreator() == null) {
				setCreator(log.getOperator());
			}
		}
	}

}
