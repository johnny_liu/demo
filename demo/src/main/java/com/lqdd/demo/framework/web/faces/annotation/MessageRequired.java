package com.lqdd.demo.framework.web.faces.annotation;

/**
 * 
 */
/**
 * @author liuqiang
 *
 */

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageRequired {

	MessageType type() default MessageType.OPERATION;

}