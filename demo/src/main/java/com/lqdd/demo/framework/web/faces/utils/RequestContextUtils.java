package com.lqdd.demo.framework.web.faces.utils;

import org.primefaces.context.RequestContext;

@SuppressWarnings("deprecation")
public class RequestContextUtils {

	public static void execute(String script) {
		RequestContext.getCurrentInstance().execute(script);
	}

	public static void update(String componentId) {
		RequestContext.getCurrentInstance().update(componentId);
	}

	public static void executeAndUpdate(String script, String componentId) {
		execute(script);
		update(componentId);
	}
}
