package com.lqdd.demo.framework.service;

import java.util.List;
import java.util.Map;

public interface BaseService<T> {

	public void save(T t);

	public void delete(T[] tArray);

	public T findById(Long id);

	public List<T> findAll(Map<String, Object> sreachParam);

	public T findOne(Map<String, Object> sreachParam);
}
