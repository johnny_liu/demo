package com.lqdd.demo.framework.web.faces.context;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class ViewExpiredExceptionHandlerFactory extends ExceptionHandlerFactory {
	private ExceptionHandlerFactory parent;

	@SuppressWarnings("deprecation")
	public ViewExpiredExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		this.parent = parent;
	}

	public ExceptionHandler getExceptionHandler() {
		ExceptionHandler result = new ViewExpiredExceptionHandler(this.parent.getExceptionHandler());
		return result;
	}
}
