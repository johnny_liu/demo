package com.lqdd.demo.framework.web.faces.controller;

import java.io.IOException;
import java.util.Locale;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.web.jsf.FacesContextUtils;

import com.lqdd.demo.framework.web.faces.scope.ViewScope;

public abstract class FacesController {

	public static final Logger logger = LogManager.getLogger(FacesController.class);

	@Autowired
	private ViewScope viewScope;
	@Autowired
	private MessageSource messageSource;

	public void onEntry() {
		logger.info("------ enter FacesController");
	}

	public static FacesController getCurrentInstance() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String path = facesContext.getExternalContext().getRequestServletPath();
		ApplicationContext applicationContext = FacesContextUtils.getRequiredWebApplicationContext(facesContext);
		try {
			return (FacesController) applicationContext.getBean(path, FacesController.class);
		} catch (NoSuchBeanDefinitionException e) {
		}
		return null;
	}

	public void putViewScope(String name, Object obj) {
		this.viewScope.put(name, obj);
	}

	public Object getViewScope(String name) {
		return this.viewScope.get(name);
	}

	public HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	public void redirectInternal(String url) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		redirectExternal(externalContext.getRequestContextPath() + url);
	}

	public void redirectExternal(String url) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		try {
			externalContext.redirect(url);
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	public Locale getLocale() {
		return getResponse().getLocale();
	}

	public String getMessage(String code, Object[] args) {
		return this.messageSource.getMessage(code, args, getLocale());
	}

	public String getMessage(String code, Object[] args, String defaultMessage) {
		return this.messageSource.getMessage(code, args, defaultMessage, getLocale());
	}

	protected void throwValidatorMessages(String code, Object[] args, FacesMessage.Severity severity) {
		FacesMessage facesMessage = new FacesMessage(getMessage(code, args));
		facesMessage.setSeverity(severity);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
}
