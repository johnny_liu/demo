package com.lqdd.demo.framework.web.faces.annotation;

public enum MessageType {
	OPERATION("operationSuccess", "operationFailed"), SAVE("saveSuccess", "saveFailed"), DELETE("deleteSuccess",
			"deleteFailed"), IMPORT("importSuccess", "importFailed");

	private final String successCode;
	private final String failedCode;

	private MessageType(String successCode, String failedCode) {
		this.successCode = successCode;
		this.failedCode = failedCode;
	}

	public String getSuccessCode() {
		return this.successCode;
	}

	public String getFailedCode() {
		return this.failedCode;
	}
}
