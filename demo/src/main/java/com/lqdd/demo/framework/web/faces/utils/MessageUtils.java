package com.lqdd.demo.framework.web.faces.utils;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class MessageUtils {

	public static void showSuccessMessage(String message, Severity severity) {
		showMessageGrowl(message, severity);
	}

	public static void showDialogMessage(String message, Severity severity) {
		showMessageDialog(message, severity);
	}

	public static void addGlobalMessage(String message, Severity severity) {
		addGlobalMessage(new FacesMessage(severity, message, message));
	}

	public static void addGlobalMessage(FacesMessage facesMessage) {
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}

	public static void addGlobalMessages(List<String> messages, Severity severity) {
		for (String message : messages) {
			addGlobalMessage(message, severity);
		}
	}

	public static void showMessageGrowl() {
		RequestContextUtils.update("globalMessageGrowl");
	}

	public static void showMessageDialog() {
		RequestContextUtils.executeAndUpdate("globalMessageDialog.show();", "globalMessages");
	}

	private static void showMessageGrowl(String message, Severity severity) {
		addGlobalMessage(message, severity);
		showMessageGrowl();
	}

	private static void showMessageDialog(String message, Severity severity) {
		addGlobalMessage(message, severity);
		showMessageDialog();
	}
}
