package com.lqdd.demo.framework.web.faces.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.event.TabCloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.lqdd.demo.security.domain.Resource;
import com.lqdd.demo.security.domain.Resource.Type;
import com.lqdd.demo.security.service.ResourceService;

@Controller("/index.faces")
public class IndexController extends FacesController {

	@Autowired
	private ResourceService resourceService;

	@Override
	public void onEntry() {
		putViewScope("activeIndex", 0);
		putViewScope("resources", findMenu());
		putViewScope("selectedResources", new ArrayList<Resource>());
	}

	private List<Resource> findMenu() {
		Map<String, Object> sreachParam = new HashMap<>();
		sreachParam.put("type", Type.MENU);
		return resourceService.findInfo(sreachParam);
	}

	@SuppressWarnings("unchecked")
	public void addTabs(Resource resource) {
		ArrayList<Resource> selectedResources = (ArrayList<Resource>) getViewScope("selectedResources");
		int activeIndex = 0;
		if (!selectedResources.contains(resource)) {
			selectedResources.add(resource);
			activeIndex = selectedResources.size() - 1;
		} else {
			activeIndex = selectedResources.indexOf(resource);
		}
		putViewScope("activeIndex", activeIndex);
	}

	@SuppressWarnings("unchecked")
	public void onTabClose(TabCloseEvent event) {
		ArrayList<Resource> selectedResources = (ArrayList<Resource>) getViewScope("selectedResources");
		selectedResources.remove(event.getData());
	}
}
