package com.lqdd.demo.framework.web.faces.scope;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;

@Component("viewScope")
public class ViewScope implements Map<String, Object> {
	public void clear() {
		getViewMap().clear();
	}

	public boolean containsKey(Object key) {
		return getViewMap().containsKey(key);
	}

	public boolean containsValue(Object value) {
		return getViewMap().containsValue(value);
	}

	public Set<Map.Entry<String, Object>> entrySet() {
		return getViewMap().entrySet();
	}

	public Object get(Object key) {
		return getViewMap().get(key);
	}

	public boolean isEmpty() {
		return getViewMap().isEmpty();
	}

	public Set<String> keySet() {
		return getViewMap().keySet();
	}

	public Object put(String key, Object value) {
		return getViewMap().put(key, value);
	}

	public void putAll(Map<? extends String, ? extends Object> m) {
		getViewMap().putAll(m);
	}

	public Object remove(Object key) {
		return getViewMap().remove(key);
	}

	public int size() {
		return getViewMap().size();
	}

	public Collection<Object> values() {
		return getViewMap().values();
	}

	private Map<String, Object> getViewMap() {
		return FacesContext.getCurrentInstance().getViewRoot().getViewMap();
	}
}
