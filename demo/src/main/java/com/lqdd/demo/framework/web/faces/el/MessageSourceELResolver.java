package com.lqdd.demo.framework.web.faces.el;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.PropertyNotWritableException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.web.jsf.FacesContextUtils;

public class MessageSourceELResolver extends ELResolver {
	static final String RESOURCE_BUNDLE_KEY = "msg";

	public Class<?> getCommonPropertyType(ELContext context, Object base) {
		if (base == null) {
			return MessageSource.class;
		}
		if ((base instanceof MessageSource)) {
			return String.class;
		}
		return null;
	}

	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
		return null;
	}

	public Class<?> getType(ELContext context, Object base, Object property) {
		if ((base == null) && (RESOURCE_BUNDLE_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return MessageSource.class;
		}
		if ((base instanceof MessageSource)) {
			context.setPropertyResolved(true);
			return String.class;
		}
		return null;
	}

	public Object getValue(ELContext context, Object base, Object property) {
		if ((base == null) && (RESOURCE_BUNDLE_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return getMessageSource();
		}
		if ((base instanceof MessageSource)) {
			context.setPropertyResolved(true);
			MessageSource messageSource = (MessageSource) base;
			String message = messageSource.getMessage(property.toString(), null, null, getResponse().getLocale());
			if (message != null) {
				return message;
			}
			return property.toString();
		}
		return null;
	}

	public boolean isReadOnly(ELContext context, Object base, Object property) {
		if ((base == null) && (RESOURCE_BUNDLE_KEY.equals(property))) {
			context.setPropertyResolved(true);
			return true;
		}
		if ((base instanceof MessageSource)) {
			context.setPropertyResolved(true);
			return true;
		}
		return false;
	}

	public void setValue(ELContext context, Object base, Object property, Object value) {
		if ((base == null) && (RESOURCE_BUNDLE_KEY.equals(property))) {
			throw new PropertyNotWritableException("The 'msg' variable is not writable.");
		}
		if ((base instanceof MessageSource)) {
			throw new PropertyNotWritableException("The MessageSource is not writable.");
		}
	}

	protected MessageSource getMessageSource() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return FacesContextUtils.getRequiredWebApplicationContext(facesContext);
	}

	private HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}
}
