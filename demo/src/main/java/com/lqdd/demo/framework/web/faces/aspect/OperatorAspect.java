package com.lqdd.demo.framework.web.faces.aspect;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import com.lqdd.demo.common.utils.UserUtil;
import com.lqdd.demo.framework.domain.AuditableEntity;

@Aspect
@Configuration
public class OperatorAspect {
	@Pointcut(value = "execution(* com.lqdd.demo.persistence.BaseDao.*.*(..))")
	public void pointcut() {
	}

	@Before(value = "pointcut() && args(entity)")
	public void doSomething(AuditableEntity entity) {
		System.out.println(entity);
	}

	@Around("pointcut()")
	public Object aroundMethod(ProceedingJoinPoint pjp) throws Throwable {
		Object[] args = pjp.getArgs();
		for (Object obj : args) {// 查看参数值
			if (obj instanceof AuditableEntity) {
				AuditableEntity entity = (AuditableEntity) obj;
				entity.setCreatedTime(new Date());
				entity.setOperator(UserUtil.getLoginName());
			}
		}
		return pjp.proceed();
	}

}
