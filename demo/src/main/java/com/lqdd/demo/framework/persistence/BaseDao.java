package com.lqdd.demo.framework.persistence;

import java.util.List;
import java.util.Map;

import com.lqdd.demo.framework.domain.BaseEntity;

public interface BaseDao<T extends BaseEntity> {

	public int insert(T t);

	public int update(T t);

	public int delete(Long id);

	public List<T> query(Map<String, Object> param);

	public T get(Long id);
}