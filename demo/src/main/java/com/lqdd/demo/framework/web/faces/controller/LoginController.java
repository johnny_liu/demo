package com.lqdd.demo.framework.web.faces.controller;

import javax.faces.application.FacesMessage;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;

@Controller("/login.faces")
public class LoginController extends FacesController {

	@Override
	public void onEntry() {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {
			redirectInternal("/index.faces");
		}
	}

	public void onLogin() {
		String username = (String) getViewScope("username");
		String password = (String) getViewScope("password");
		Boolean rememberMe = (Boolean) getViewScope("rememberMe");
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
		try {
			subject.login(token);
			if (subject.isAuthenticated()) {
				redirectInternal("/index.faces");
			}
		} catch (UnknownAccountException ex) {
			throwValidatorMessages("error.usernameError", null, FacesMessage.SEVERITY_ERROR);
		} catch (IncorrectCredentialsException ex) {
			throwValidatorMessages("error.usernameOrPasswordError", null, FacesMessage.SEVERITY_ERROR);
		} catch (LockedAccountException ex) {
			throwValidatorMessages("error.contactTheAdministrator", null, FacesMessage.SEVERITY_ERROR);
		} catch (Exception ex) {
			throwValidatorMessages("error.contactTheAdministrator", null, FacesMessage.SEVERITY_ERROR);
		} finally {
			token.clear();
		}
	}
}
