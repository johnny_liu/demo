package com.lqdd.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.lqdd.demo.common.config.WebAppInitializer;

@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(new Class[] { DemoApplication.class, WebAppInitializer.class });
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
