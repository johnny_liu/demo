package com.lqdd.demo.common.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.primefaces.util.Constants;
import org.springframework.boot.web.servlet.ServletContextInitializer;

public class WebAppInitializer implements ServletContextInitializer {

	@Override
	public void onStartup(ServletContext context) throws ServletException {
		context.setInitParameter(Constants.ContextParams.FONT_AWESOME, "true");
		context.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
		context.setInitParameter("javax.faces.PARTIAL_STATE_SAVING_METHOD", "true");
		// 项目阶段（Development）开发还是（Production）产品
		context.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
		// 用于配置把视图状态数据放在服务器端还是传递到客户端
		context.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
		context.setInitParameter("javax.faces.FACELETS_REFRESH_PERIOD", "1");
		context.setInitParameter("facelets.DEVELOPMENT", "true");
		// 主题 omega
		context.setInitParameter("primefaces.THEME", "omega");
		context.setInitParameter("primefaces.FONT_AWESOME", "true");
			
	}
}
