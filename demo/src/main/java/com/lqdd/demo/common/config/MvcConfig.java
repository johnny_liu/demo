package com.lqdd.demo.common.config;

import java.util.Locale;

import javax.faces.webapp.FacesServlet;

import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.lqdd.demo.common.listener.JsfConfigureListener;

@Configuration
public class MvcConfig extends WebMvcConfigurationSupport {

	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		FacesServlet servlet = new FacesServlet();
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.faces");
		return servletRegistrationBean;
	}

	// 注册Listener
	@Bean
	public ServletListenerRegistrationBean<JsfConfigureListener> jsfConfigureListener() {
		ServletListenerRegistrationBean<JsfConfigureListener> listenerRegistrationBean = new ServletListenerRegistrationBean<>(
				new JsfConfigureListener());
		return listenerRegistrationBean;
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/views/");
		viewResolver.setSuffix(".xhtml");
		return viewResolver;
	}

	@Bean
	public MimeMappings mimeMappings() {
		MimeMappings mappings = new MimeMappings();
		mappings.add("ttf", "application/x-font-ttf");
		mappings.add("woff", "application/font-woff");
		mappings.add("woff2", "application/font-woff2");
		mappings.add("eot", "application/vnd.ms-fontobject");
		mappings.add("eot?#iefix", "application/vnd.ms-fontobject");
		mappings.add("svg", "image/svg+xml");
		mappings.add("svg#exosemibold", "image/svg+xml");
		mappings.add("svg#exobolditalic", "image/svg+xml");
		mappings.add("svg#exomedium", " image/svg+xml");
		mappings.add("svg#exoregular", "image/svg+xml");
		mappings.add("svg#fontawesomeregular", " mage/svg+xml");
		return mappings;
	}

	 @Bean  
     public LocaleResolver localeResolver() {  
         SessionLocaleResolver slr = new SessionLocaleResolver();  
         slr.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);  
         return slr;  
     }  

     @Bean  
     public LocaleChangeInterceptor localeChangeInterceptor() {  
         LocaleChangeInterceptor lci = new LocaleChangeInterceptor();  
         lci.setParamName("lang");  
         return lci;  
     }  

     @Override  
     public void addInterceptors(InterceptorRegistry registry) {  
         registry.addInterceptor(localeChangeInterceptor());  
     }  

	
	

	// @Bean
	// public ResourceBundleMessageSource resourceBundleMessageSource() {
	// ResourceBundleMessageSource messageSource = new
	// ResourceBundleMessageSource();
	// messageSource.setBasename("/config/messages/messages_cn");
	// messageSource.setUseCodeAsDefaultMessage(true);
	// return messageSource;
	// }

	// @Bean
	// public ServletContextTemplateResolver servletContextTemplateResolver() {
	// ServletContextTemplateResolver templateResolver = new
	// ServletContextTemplateResolver();
	// templateResolver.setPrefix("classpath:/views/");
	// templateResolver.setSuffix(".xhtml");
	// templateResolver.setTemplateMode("XHTML");
	// templateResolver.setCharacterEncoding("UTF-8");
	// return templateResolver;
	// }
}
