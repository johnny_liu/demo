/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.common.listener;

import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.context.ExceptionHandlerFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContextEvent;

import org.springframework.web.jsf.el.SpringBeanFacesELResolver;

import com.lqdd.demo.framework.web.faces.context.ViewExpiredExceptionHandlerFactory;
import com.lqdd.demo.framework.web.faces.el.FacesControllerELResolver;
import com.lqdd.demo.framework.web.faces.el.PlatformMessageSourceELResolver;
import com.lqdd.demo.framework.web.faces.event.ControllerPhaseListener;
import com.sun.faces.config.ConfigureListener;

public class JsfConfigureListener extends ConfigureListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		super.contextInitialized(sce);
		// application
		ApplicationFactory appFactory = (ApplicationFactory) FactoryFinder
				.getFactory(FactoryFinder.APPLICATION_FACTORY);
		Application app = appFactory.getApplication();
		app.addELResolver(new SpringBeanFacesELResolver());
		app.addELResolver(new FacesControllerELResolver());
		app.addELResolver(new PlatformMessageSourceELResolver());
		app.addComponent("org.apache.shiro.web.faces.tags.PrincipalTag",
				"org.apache.shiro.web.faces.tags.PrincipalTag");
		
		// Lifecycle
		LifecycleFactory lifeFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
		Lifecycle lifecycle = lifeFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
		lifecycle.addPhaseListener(new ControllerPhaseListener());
		// HandlerFactory
		ExceptionHandlerFactory handlerFactory = (ExceptionHandlerFactory) FactoryFinder
				.getFactory(FactoryFinder.EXCEPTION_HANDLER_FACTORY);

		new ViewExpiredExceptionHandlerFactory(handlerFactory);

	}
}