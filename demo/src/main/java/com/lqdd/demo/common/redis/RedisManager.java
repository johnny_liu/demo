/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.common.redis;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.lqdd.demo.common.redis.serializer.Serializer;
import com.lqdd.demo.framework.web.faces.controller.FacesController;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

@Service
public class RedisManager {
	public static final Logger logger = LogManager.getLogger(FacesController.class);

	private static JedisPool pool;

	private static RedisConfig redisConfig;

	public static final int EXRP_HOUR = 60 * 60; // 一小时
	public static final int EXRP_DAY = 60 * 60 * 24; // 一天
	public static final int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

	private static void initialPool() {
		redisConfig = RedisConfigurationBuilder.getInstance().parseConfiguration();
		pool = new JedisPool(redisConfig, redisConfig.getHost(), redisConfig.getPort(),
				redisConfig.getConnectionTimeout(), redisConfig.getSoTimeout(), redisConfig.getPassword(),
				redisConfig.getDatabase(), redisConfig.getClientName(), redisConfig.isSsl(),
				redisConfig.getSslSocketFactory(), redisConfig.getSslParameters(), redisConfig.getHostnameVerifier());
	}

	private static void poolInit() {
		if (pool == null) {
			initialPool();
		}
	}

	public static Jedis getJedis() {
		Jedis jedis = null;
		try {
			poolInit();
			jedis = pool.getResource();
		} catch (Exception e) {
			throw new JedisConnectionException(e);
		}
		return jedis;
	}

	private static <T> T execute(RedisCallback<T> callback) {
		Jedis jedis = getJedis();
		try {
			return callback.doWithRedis(jedis);
		} catch (Exception e) {
			throw new JedisConnectionException(e);
		} finally {
			jedis.close();
		}
	}

	// 插入字符串
	public static boolean set(String key, Object value) {
		boolean result = execute(new RedisCallback<Boolean>() {
			@Override
			public Boolean doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				jedis.set(serializer.serialize(key), serializer.serialize(value));
				return true;
			}
		});
		return result;
	}

	// 获取字符串
	public static Object get(String key) {
		execute(new RedisCallback<Object>() {
			@Override
			public Object doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				byte[] value = jedis.get(serializer.serialize(key));
				return serializer.unserialize(value);
			}
		});
		return null;
	}

	// 删除字符串
	public static Long del(String id) {
		long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				Long res = jedis.del(serializer.serialize(id));
				return res;
			}
		});
		return result;
	}

	public static Long expire(String key, long expire, int timeUnit) {
		Long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				return jedis.expire(serializer.serialize(key), (int) expire * timeUnit);
			}
		});
		return result;
	}

	// 将一个或多个值插入到列表头部
	public static long lpush(final String key, Object value) {
		long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				long count = jedis.lpush(serializer.serialize(key), serializer.serialize(value));
				return count;
			}
		});
		return result;
	}

	// 在列表中添加一个或多个值
	public static long rpush(final String key, Object value) {
		long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				long count = jedis.rpush(serializer.serialize(key), serializer.serialize(value));
				return count;
			}
		});
		return result;
	}

	// 移出并获取列表的第一个元素
	public static Object lpop(final String key) {
		Object result = execute(new RedisCallback<Object>() {
			@Override
			public Object doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				byte[] res = jedis.lpop(serializer.serialize(key));
				return serializer.unserialize(res);
			}
		});
		return result;
	}

	// 获取在哈希表中指定 key 的所有字段和值
	public static Map<byte[], byte[]> hGetAll(String id) {
		Map<byte[], byte[]> result = execute(new RedisCallback<Map<byte[], byte[]>>() {
			@Override
			public Map<byte[], byte[]> doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				Map<byte[], byte[]> res = jedis.hgetAll(serializer.serialize(id));
				return res;
			}
		});
		return result;
	}

	// 将哈希表 key 中的字段 field 的值设为 value 。
	public static long hset(String id, Object key, Object value) {
		long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				Long res = jedis.hset(serializer.serialize(id), serializer.serialize(key), serializer.serialize(value));
				return res;
			}
		});
		return result;
	}

	// 获取存储在哈希表中指定字段的值。
	public static Object hget(String id, Object key) {
		Object result = execute(new RedisCallback<Object>() {
			@Override
			public Object doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				byte[] res = jedis.hget(serializer.serialize(id), serializer.serialize(key));
				return serializer.unserialize(res);
			}
		});
		return result;
	}

	// 删除一个或多个哈希表字段
	public static Long hdel(String id, Object key) {
		Long result = execute(new RedisCallback<Long>() {
			@Override
			public Long doWithRedis(Jedis jedis) throws DataAccessException {
				Serializer serializer = redisConfig.getSerializer();
				Long res = jedis.hdel(serializer.serialize(id), serializer.serialize(key));
				return res;
			}
		});
		return result;
	}

}