package com.lqdd.demo.common.shiro.cache;

import java.io.Serializable;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;

import com.lqdd.demo.common.redis.RedisManager;

public class SessionRedisDao extends EnterpriseCacheSessionDAO {
	@Override
	protected Serializable doCreate(Session session) {
		Serializable sessionId = super.doCreate(session);
		RedisManager.set(session.getId().toString(), session);
		return sessionId;
	}

	// 获取session
	@Override
	protected Session doReadSession(Serializable sessionId) {
		// 先从缓存中获取session，如果没有再去数据库中获取
		Session session = super.doReadSession(sessionId);
		if (session == null) {
			session = (Session) RedisManager.get(sessionId.toString());
		}
		return session;
	}

	// 更新session的最后一次访问时间
	@Override
	protected void doUpdate(Session session) {
		super.doUpdate(session);
		RedisManager.set(session.getId().toString(), session);
	}

	// 删除session
	@Override
	protected void doDelete(Session session) {
		super.doDelete(session);
		RedisManager.del(session.getId().toString());
	}

}
