package com.lqdd.demo.common.redis.serializer;

public interface Serializer {
	/**
	 * 序列化
	 * 
	 * @param object
	 * @return serialized bytes
	 */
	public byte[] serialize(Object object);

	/**
	 * 反序列化
	 * 
	 * @param bytes
	 * @return unserialized object
	 */
	public Object unserialize(byte[] bytes);
}
