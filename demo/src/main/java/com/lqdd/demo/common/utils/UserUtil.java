/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.lqdd.demo.common.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.lqdd.demo.security.domain.User;

public class UserUtil {

	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}

	public static User getLoginUser() {
		try {
			Subject subject = SecurityUtils.getSubject();
			User principal = (User) subject.getSession().getAttribute("shiroUser");
			if (principal != null) {
				return principal;
			}
			// subject.logout();
		} catch (UnavailableSecurityManagerException e) {

		} catch (InvalidSessionException e) {

		}
		return null;
	}

	public static String getLoginName() {
		User user = getLoginUser();
		if (user != null) {
			return user.getUsername();
		}
		return "";
	}

	/**
	 * 获取当前会话
	 * 
	 * @return Session
	 */
	public static Session getSession() {
		try {
			Subject subject = SecurityUtils.getSubject();
			Session session = subject.getSession(false);
			if (session == null) {
				session = subject.getSession();
			}
			if (session != null) {
				return session;
			}
			// subject.logout();
		} catch (InvalidSessionException e) {

		}
		return null;
	}

}
