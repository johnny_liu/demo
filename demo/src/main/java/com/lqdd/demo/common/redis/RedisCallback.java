package com.lqdd.demo.common.redis;

import redis.clients.jedis.Jedis;

public interface RedisCallback<T> {
	T doWithRedis(Jedis jedis);
}
