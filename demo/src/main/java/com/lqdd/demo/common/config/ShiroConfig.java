
package com.lqdd.demo.common.config;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lqdd.demo.common.shiro.Filter.MyFormAuthenticationFilter;
import com.lqdd.demo.common.shiro.cache.SessionRedisDao;
import com.lqdd.demo.common.shiro.realm.UserRealm;
import com.lqdd.demo.security.service.ResourceService;

@Configuration
public class ShiroConfig {

	@Autowired
	private ResourceService resourceService;

	public static final String PREMISSION_STRING = "perms[\"{0}\"]";

	public static final int KEEP_DAYS = 30;

	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
		// 必须配置 SecurityManager
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager);
		// 添加自定义拦截器
		addMyFilter(shiroFilterFactoryBean);
		// 拦截配置
		Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
		addMyResourceFilter(filterChainDefinitionMap);

		shiroFilterFactoryBean.setLoginUrl("/login.faces");
		shiroFilterFactoryBean.setSuccessUrl("/index.faces");
		shiroFilterFactoryBean.setUnauthorizedUrl("/access.faces");

		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

		return shiroFilterFactoryBean;
	}

	public void addMyResourceFilter(Map<String, String> filterChainDefinitionMap) {
		/*
		 * 过滤链定义： 拦截顺序：从上到下 一般将 /** 放在最下边，拦截所有连接，表示最后拦截
		 */
		// anon 表示不拦截

		filterChainDefinitionMap.put("/javax.faces.resource/**", "anon");
		filterChainDefinitionMap.put("/login.faces", "anon");
		filterChainDefinitionMap.put("/index.faces", "authc");
		filterChainDefinitionMap.put("/logout", "logout");

		// 资源权限
		resourceService.findShiroPermissions(filterChainDefinitionMap);
		// authc 表示拦截认证后才能访问
		filterChainDefinitionMap.put("/**", "authc");
	}

	private void addMyFilter(ShiroFilterFactoryBean shiroFilterFactoryBean) {
		Map<String, Filter> filters = new LinkedHashMap<>();
		filters.put("authc", new MyFormAuthenticationFilter());
		shiroFilterFactoryBean.setFilters(filters);
	}

	@Bean
	public SecurityManager securityManager() {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		// 注入自定义的Realm
		securityManager.setRealm(shiroDbRealm());
		// 注入缓存处理器
		// securityManager.setCacheManager(ehCacheManager());

		securityManager.setSessionManager(sessionManager());
		// 注入记住账号处理器
		securityManager.setRememberMeManager(rememberMeManager());

		return securityManager;
	}

	@Bean
	public UserRealm shiroDbRealm() {
		UserRealm shiroDbRealm = new UserRealm();
		shiroDbRealm.setCredentialsMatcher(hashedCredentialsMatcher());
		return shiroDbRealm;
	}

	/**
	 * 定义凭证匹配器 用于登录密码验证
	 * 
	 * @return
	 */
	@Bean
	public HashedCredentialsMatcher hashedCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
		matcher.setHashAlgorithmName("SHA-1");// 散列算法
		matcher.setHashIterations(1024);
		return matcher;
	}

	@Bean
	@ConditionalOnMissingBean
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);

		return defaultAdvisorAutoProxyCreator;
	}

	/**
	 * 开启 Shiro AOP 注解支持
	 * 
	 * @param securityManager
	 * @return
	 */
	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);

		return authorizationAttributeSourceAdvisor;
	}

	/**
	 * Shiro 缓存管理器
	 * 
	 * @return
	 */
	// @Bean
	// public EhCacheManager ehCacheManager() {
	// EhCacheManager ehCacheManager = new EhCacheManager();
	// ehCacheManager.setCacheManagerConfigFile("classpath:ehcache.xml");
	// return ehCacheManager;
	// }

	@Bean
	public CookieRememberMeManager rememberMeManager() {
		CookieRememberMeManager rememberMeManager = new CookieRememberMeManager();
		SimpleCookie cookie = new SimpleCookie("rememberMe");
		cookie.setHttpOnly(true);
		cookie.setMaxAge(60 * 60 * 24 * KEEP_DAYS);
		rememberMeManager.setCookie(cookie);
		return rememberMeManager;
	}

	/**
	 * shiro session的管理
	 */
	@Bean
	public DefaultWebSessionManager sessionManager() {
		DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
		sessionManager.setSessionDAO(new SessionRedisDao());
		return sessionManager;
	}

}
