
package com.lqdd.demo.common.utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.ClassUtils;

public class EnumUtil {
	public static String[] enumsToStringArray(Enum<?>[] enumerations) {
		String[] strings = new String[enumerations.length];
		for (int i = 0; i < enumerations.length; i++) {
			strings[i] = enumerations[i].toString();
		}
		return strings;
	}

	public static String getMessage(Enum<?> enumeration, String bundleBaseName) {
		ResourceBundle bundle = ResourceBundle.getBundle(bundleBaseName);
		return bundle.getString(getMessageKey(enumeration));
	}

	public static Map<Locale, String> getLocalizedMessages(Enum<?> enumeration, String bundleBaseName,
			Locale[] locales) {
		Map<Locale, String> localizedMessages = new HashMap<>();
		Locale[] arrayOfLocale;
		int j = (arrayOfLocale = locales).length;
		for (int i = 0; i < j; i++) {
			Locale locale = arrayOfLocale[i];
			ResourceBundle bundle = ResourceBundle.getBundle(bundleBaseName, locale);
			localizedMessages.put(locale, bundle.getString(getMessageKey(enumeration)));
		}
		return localizedMessages;
	}

	private static String getMessageKey(Enum<?> enumeration) {
		return enumeration != null ? ClassUtils.getShortCanonicalName(enumeration.getClass()) + "." + enumeration.name()
				: "";
	}
}