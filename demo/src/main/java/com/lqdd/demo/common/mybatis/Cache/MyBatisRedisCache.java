/**
 * 
 */
/**
 * @author liuqiang
 *
 */
package com.lqdd.demo.common.mybatis.Cache;

import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.ibatis.cache.Cache;

import com.lqdd.demo.common.redis.DummyReadWriteLock;
import com.lqdd.demo.common.redis.RedisManager;

public class MyBatisRedisCache implements Cache {

	private final ReadWriteLock readWriteLock = new DummyReadWriteLock();

	private Integer timeout;

	private String id;

	public MyBatisRedisCache(final String id) {
		if (id == null) {
			throw new IllegalArgumentException("Cache instances require an ID");
		}
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void putObject(Object key, Object value) {
		RedisManager.hset(id, key, value);
		if (timeout != null) {
			RedisManager.expire(id, timeout, RedisManager.EXRP_HOUR);
		}
	}

	@Override
	public Object getObject(Object key) {
		return RedisManager.hget(id, key);
	}

	@Override
	public Object removeObject(Object key) {
		return RedisManager.hdel(id, key);
	}

	@Override
	public void clear() {
		RedisManager.del(id);
	}

	@Override
	public int getSize() {
		Map<byte[], byte[]> result = RedisManager.hGetAll(id);
		return result.size();
	}

	@Override
	public ReadWriteLock getReadWriteLock() {
		return readWriteLock;
	}

	@Override
	public String toString() {
		return "Redis {" + id + "}";
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

}