package com.lqdd.demo.common.shiro.Filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lqdd.demo.security.domain.User;
import com.lqdd.demo.security.service.UserService;

public class MyFormAuthenticationFilter extends FormAuthenticationFilter {

	@Autowired
	private UserService userService;

	// 用户超时重新登入同一登入到index页面
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,
			ServletResponse response) throws Exception {
		WebUtils.getAndClearSavedRequest(request);
		WebUtils.redirectToSavedRequest(request, response, "/index.faces");
		return false;
	}

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		Subject subject = getSubject(request, response);
		Session session = subject.getSession();
		if (subject.isAuthenticated() && subject.isRemembered() && session.getAttribute("shiroUser") == null) {
			Object principal = subject.getPrincipal();
			if (principal != null) {
				String username = principal.toString();
				User user = userService.findByUsername(username);
				session.setAttribute("shiroUser", user);
			}
		}
		return subject.isAuthenticated() || subject.isRemembered();
	}
}
