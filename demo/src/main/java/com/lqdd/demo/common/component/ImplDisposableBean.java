package com.lqdd.demo.common.component;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;

@Component
public class ImplDisposableBean implements DisposableBean, ExitCodeGenerator {
	@Override
	public void destroy() throws Exception {
		LogManager.shutdown();
	}

	@Override
	public int getExitCode() {
		return 0;
	}
}
