package org.primefaces.component.datatable.feature;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.datatable.DataTableRenderer;
import org.primefaces.util.ComponentUtils;

public class SelectionFeature implements DataTableFeature {

    public void decode(FacesContext context, DataTable table) {
        String clientId = table.getClientId(context);
        Map<String, String> params = context.getExternalContext()
                .getRequestParameterMap();

        String selection = params.get(clientId + "_selection");

        if (table.isSingleSelectionMode())
            decodeSingleSelection(table, selection);
        else
            decodeMultipleSelection(table, selection);
    }

    void decodeSingleSelection(DataTable table, String selection) {
        if (ComponentUtils.isValueBlank(selection))
            table.setSelection(null);
        else
            table.setSelection(table.getRowData(selection));
    }

    void decodeMultipleSelection(DataTable table, String selection) {
        // Bug fix - begin
        Class<?> clazz = table.getValueExpression("selection")
                .getValue(FacesContext.getCurrentInstance().getELContext())
                .getClass();
        // Bug fix - end

        if (ComponentUtils.isValueBlank(selection)) {
            Object data = Array.newInstance(clazz.getComponentType(), 0);
            table.setSelection(data);
        } else {
            String[] rowKeys = selection.split(",");
            List<Object> selectionList = new ArrayList<Object>();

            for (int i = 0; i < rowKeys.length; i++) {
                Object rowData = table.getRowData(rowKeys[i]);

                if (rowData != null)
                    selectionList.add(rowData);
            }

            Object selectinArray = Array.newInstance(clazz.getComponentType(),
                    selectionList.size());
            table.setSelection(selectionList.toArray((Object[]) selectinArray));
        }
    }

    public void encode(FacesContext context, DataTableRenderer renderer,
            DataTable table) throws IOException {
        throw new RuntimeException("SelectFeature should not encode.");
    }

    public boolean shouldDecode(FacesContext context, DataTable table) {
        return table.isSelectionEnabled();
    }

    public boolean shouldEncode(FacesContext context, DataTable table) {
        return false;
    }
}
